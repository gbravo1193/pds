package com.mitocode.spring;

import org.springframework.context.support.*;

import com.mitocode.beans.AppConfig;
import com.mitocode.beans.AppConfig2;
import com.mitocode.beans.Mundo;
import com.mitocode.beans.Persona;

import org.springframework.context.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		//ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class,AppConfig2.class);
		/**
		Mundo m = (Mundo) appContext.getBean("mundo");
		System.out.println(m.getSaludo());
		((ConfigurableApplicationContext)appContext).close();**/
		
		Persona p = (Persona) appContext.getBean("persona");
		System.out.println("id: "+p.getId() + " nombre: "+ p.getNombre() + " apodo: "+ p.getApodo()+" "+p.getPais());
		((ConfigurableApplicationContext)appContext).close();
		
		
		
	}

}
