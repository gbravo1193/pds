package idinterfaz;

public class Test {
	
	public static void main(String[]args){
		Messi m = new Messi(new Barcelona());
		m.mostrarEquipo();
		// Se cambia de equipo
		Messi n = new Messi(new Juventus());
		n.mostrarEquipo();
	}
	
}
