package com.mitocode.beans;

public class Ciudad {

	private String nombre;

	public String getNombre() {
		return nombre;
	}
	
	/*private void initBean(){
		System.out.println("Antes de inicializar el bean");
	}
	
	private void destroyBean(){
		System.out.println("Bean a punto de ser destruido");
	}*/

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
