package com.mitocode.video25;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Marca;
import com.mitocode.service.ServiceMarca;

public class App {
	public static void main(String[]args){
		Marca mar = new Marca();
		mar.setId(5);
		mar.setNombre("Marca 5");
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		ServiceMarca sm = (ServiceMarca) appContext.getBean("serviceMarcaImpl");
		try{
			sm.registrar(mar);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
