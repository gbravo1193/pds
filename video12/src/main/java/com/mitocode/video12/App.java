package com.mitocode.video12;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Ciudad;
import com.mitocode.beans.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[]args){
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		Persona per = (Persona) appContext.getBean("persona");
		Persona per2 = (Persona) appContext.getBean("persona");
		per.setId(1);
		per.setNombre("Jaime");
		per.setApodo("Mitocode");
		System.out.println(per.getId() + " "+ per.getNombre() + " "+ per.getApodo() + " " + per.getPais().getNombre()+" "+ per.getCiudad().getNombre());
		System.out.println(per2.getId() + " "+ per2.getNombre() + " "+ per2.getApodo() + " " + per2.getPais().getNombre()+" "+ per2.getCiudad().getNombre());
		//Singleton objetos iguales
		//Prototype objetos distintas

		//System.out.println(per);
		//System.out.println(per2);
	
		((ConfigurableApplicationContext)appContext).close();
	}
}
