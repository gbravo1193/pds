package com.mitocode.video10;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Ciudad;
import com.mitocode.beans.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[]args){
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		Persona per = (Persona) appContext.getBean("persona");
		String nombreCiudades="";
		for(Ciudad ciu : per.getPais().getCiudades()){
			nombreCiudades += ciu.getNombre() + "-";
		}
		System.out.println(per.getId() + " "+ per.getNombre() + " "+ nombreCiudades);
		((ConfigurableApplicationContext)appContext).close();
	}
}
