package com.mitocode.video18;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Ciudad;
import com.mitocode.beans.Jugador;
import com.mitocode.beans.Persona;
import com.mitocode.interfaces.IEquipo;

public class App {

		public static void main(String[]args){
			
			ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
			// Jugador jug = (Jugador) appContext.getBean("messi");
			IEquipo eq = (IEquipo) appContext.getBean("barcelona");
			
			//System.out.println(jug.getNombre()+" - "+jug.getEquipo().mostrar());
			System.out.println(eq.mostrar());
			
			((ConfigurableApplicationContext)appContext).close();
		}
	}
