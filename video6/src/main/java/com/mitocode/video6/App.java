package com.mitocode.video6;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[]args){
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		Persona per = (Persona) appContext.getBean("persona");
		System.out.println(per.getId() + " "+ per.getNombre() + " "+ per.getApodo() + " " );
		((ConfigurableApplicationContext)appContext).close();
	}
}
