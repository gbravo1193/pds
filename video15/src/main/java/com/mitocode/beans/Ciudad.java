package com.mitocode.beans;

public class Ciudad {
	private String nombre;

	private void init(){
		System.out.println("Antes de iniciar el bean");
	}
	
	private void destroy(){
		System.out.println("bean a punto de ser destruido");
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
