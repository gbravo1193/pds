package com.mitocode.video15;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Ciudad;
import com.mitocode.beans.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[]args){
		//con lazy-init=true
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		Persona per = (Persona) appContext.getBean("persona");
		Ciudad ciu = (Ciudad) appContext.getBean("ciudad");

		System.out.println(per.getApodo());
		System.out.println(ciu.getNombre());

		// si se borra ((ConfigurableApplica... no se destruye el bean
		((ConfigurableApplicationContext)appContext).close();
	}
}
