package com.mitocode.video5;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.AppConfig;
import com.mitocode.beans.AppConfig2;
import com.mitocode.beans.Mundo;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[]args){
		AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext();
		appContext.register(AppConfig.class);
		appContext.register(AppConfig2.class);
		appContext.refresh(); 
		Mundo m = (Mundo) appContext.getBean("marte");
		System.out.println(m.getSaludo());
		((ConfigurableApplicationContext)appContext).close();
	}
}
