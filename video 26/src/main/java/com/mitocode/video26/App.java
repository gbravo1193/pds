package com.mitocode.video26;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Jugador;
import com.mitocode.beans.Marca;
import com.mitocode.service.ServiceJugador;
import com.mitocode.service.ServiceMarca;

public class App {
	public static void main(String[]args){
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		ServiceJugador sm = (ServiceJugador) appContext.getBean("serviceJugadorImpl");
		Jugador jugador = (Jugador) appContext.getBean("jugador1");
		try{
			sm.registrar(jugador);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
