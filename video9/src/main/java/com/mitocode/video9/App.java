package com.mitocode.video9;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mitocode.beans.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[]args){
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mitocode/xml/beans.xml");
		Persona per = (Persona) appContext.getBean("personaBean");
		System.out.println(per.getId() + " "+ per.getNombre() + " "+ per.getApodo()+" "+ per.getPais().getNombre() +" "+  per.getPais().getCiudad().getNombre());
		((ConfigurableApplicationContext)appContext).close();
	}
}
